global.fetch = require('node-fetch');

const fs = require('fs'); // file system
const Discord = require('discord.js'); // discord API wrapper
const config = require('./config.js'); // App Configuration & constants
const CommandHandlers = require('./src/command-handlers.js'); // Handle client requests

const client = new Discord.Client();


client.on('ready', () => {
    console.log("Ready");
   
    // Create required directories if they don't exist.
    if (!fs.existsSync(config.tmpDir)){
        fs.mkdirSync(config.tmpDir);
    }
    if (!fs.existsSync(config.saveDir)){
        fs.mkdirSync(config.saveDir);
    }

});

client.on('message', message => {
    
    // Ignore bots
    if(message.author.bot) return;

    const commandPrefix = getCommandPrefix(message);

    if(commandPrefix !== false) {
        const args = message.content.slice(commandPrefix.length).trim().split(" ");
        const command = args.shift().toLowerCase();
        // Replace "me" with the proper username. This way a user can reference themselves in commands.
        if(args.indexOf("me") > -1) {
            args[args.indexOf("me")] = message.author.username;
        }

        //message.channel.send("Command: " + command);
        //message.channel.send("Args: " + JSON.stringify(args));

        switch(command) {
            case "wordcloud": {
                CommandHandlers.handleWordCloudCommand(message, args, sendResponse);
                break;
            }
            case "cleanup": {
                CommandHandlers.handleCleanupCommand(message, args, sendResponse);
                break;
            }
            default: {
                // add command back to arguments, command isn't valid.
                args.unshift(command);
                CommandHandlers.handleCryptoCommand(message, args, sendResponse);
            }
        }

        return ;
    } 
});

function sendResponse(err, message, text, data) {
    if(err) {
        console.log(err);
    }else if(text) {
        message.channel.send(text, data);
    }
}

function getCommandPrefix(message) {
    // Check if the message starts with any prefix keywords
    return config.commands.prefixes.reduce((b, prefix, i) =>{
        if(message.content.toLowerCase().startsWith(prefix)) {
            return prefix;
        }else{
            return b;
        }
    }, false);
}

client.login(config.discord.token);


