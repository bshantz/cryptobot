const config = require('../config');

const crypto = require('./crypto.js');
const R = require('r-script');
const fs = require('fs');


function getMessages(channel, lastMessageID, results, full=false) {
    console.log(lastMessageID);

    return channel.fetchMessages({before: lastMessageID, limit: 100})
        .then(messages => {
            if(!messages) {
                return results;
            }
            else{
                if(full) {
                    results = results.concat(messages.array());
                }else{
                    let cleaned = messages.map(m=>{
                        return {
                            id: m.id,
                            content: m.content,
                            username: m.author.username,
                            user_id: m.author.id,
                            created_at: m.createdTimestamp,
                            is_bot: m.author.bot
                        }
                    });
                    results = results.concat(cleaned); // 1,2,3,4,5,6
                }
                
                //return results;
                if(!messages.last()) {
                    return results;
                }
                return getMessages(channel, messages.last().id, results, full).catch(e=>{console.log(e)});
            }
        });
  }

function handleCryptoCommand(m, args, callback) {
    let username = m.author.username;
    let coins = args.map(arg=>{return crypto.findCoin(arg)}).filter(c=>{return c !== false;});
    if(coins.length === 0) {
        callback(null, null, null);
    }else{
        if(coins.length >= 1) {
            let symbols = coins.map(c => c.Symbol);

            crypto.getPriceForCoinAsync(symbols).then(results => {
                return symbols.map(key => {
                    console.log(results[key]);
                    let price = results[key].USD.PRICE;
                    let change = Math.floor(results[key].USD.CHANGEPCT24HOUR*10000)/10000;
                    return [key, "is currently trading at ", "$"+results[key].USD.PRICE, "USD. The 24H change is", change+"%"].join(" ");
                });
            })
            .then(replies => {
                callback(null, m, replies.join("\n"));
            });
        }
    }
} 

function handleWordCloudCommand(m, args, callback) {
    
    let username = (args[0]) ? args[0] : false;
   
    let userIndex = m.channel.guild.members.map(u=>u.user.username).indexOf(username);
    if(userIndex === -1) {
        callback(null, m, "No valid user was provided. Valid users are " + JSON.stringify(m.channel.guild.members.map(u=>u.user.username)));
    }else{
        getMessages(m.channel, 0, []).then(messages=>{
            fs.writeFileSync(config.tmpDir+"/"+m.channel.name+".json", JSON.stringify(messages));
                R("./r-scripts/wordcloud-generator.R").data({username: username, file: config.tmpDir+"/"+m.channel.name+".json"}).call((err, d) => {
                    if(err) {
                        callback(err);
                    }else{
                        callback(null, m, "I created a Word Cloud for " + username + " based on their activity in this channel.", {
                            file: d
                        })
                    }     
            });
        });  
    }
}

function handleCleanupCommand(m, args, callback) {
    getMessages(m.channel, 0, [], true).then(messages=>{
        return messages.filter(msg=>{
            return (msg.author.bot && msg.author.username === "cryptobot") 
            || (msg.content.startsWith("mort") || msg.content.startsWith("!"));
        });
    }).then(messagesToDelete=>{
        let chunks = [];
        while(messagesToDelete.length) {
            chunks.push(messagesToDelete.splice(0,100));
        }
       return chunks;
    }).then(batchesToDelete=>{
        Promise.all(batchesToDelete.map(b=>m.channel.bulkDelete(b))).then(results=>{
            console.log(results);
            console.log("Cleanup Complete");
        });
    });
}


module.exports = {
    handleCryptoCommand,
    handleWordCloudCommand,
    handleCleanupCommand
};