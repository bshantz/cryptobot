global.fetch = require('node-fetch');
const cc = require('cryptocompare');
const refreshTime = 60*1000; // 1 minute

var coinLookup = {};
var coinNameToKey = {};

function refreshData() {
    Promise.all(
        [cc.coinList()]
    ).then(results => {
        let coinListResults = results[0];
        if(coinListResults.Type == 100) {
            updateCoinListData(coinListResults.Data);
        }else{
            console.log(coinListResults.Type);
        }
    }).catch(err=>{
        console.log(err)
    });
}

function updateCoinListData(coinListResults) {
    coinLookup = coinListResults;
    for(var key in coinLookup) {
        coinNameToKey[coinLookup[key].CoinName] = key;
    }
}

function getCoinLookup() {
    return coinLookup;
}

function findCoinForName(name) {
    var coin = false;
    for(var key in coinLookup) {
        if(coinLookup[key].CoinName.toLowerCase() === name.toLowerCase()) {
            coin = coinLookup[key].CoinName;
        }
        break;
    }
    return coin;
}

function findCoin(str) {
    if(coinLookup.hasOwnProperty(str.toUpperCase())) {
        return coinLookup[str.toUpperCase()];
    }else{
        return findCoinForName(str) 
    }
}

function getPriceForCoinAsync(coins) {
    return cc.priceFull(coins, ['USD']);
}

refreshData()
setTimeout(refreshData, refreshTime);


module.exports = {
    findCoin,
    getPriceForCoinAsync
}